package icst349.pawcreate;

/**
 * Created by Carlo on 2017-03-13.
 */

public class User {
    private String name;
    private String email;
    private String contact_number;
    private String about_me;
    private String number_of_pets;

    User(){
    }

    User(String n, String e, String cn, String am) {
        name = n;
        email = e;
        contact_number = cn;
        about_me = am;
    }

    public void setName(String n) {
        name = n;
    }

    public void setEmail(String e) {
        email = e;
    }

    public void setContactNumber(String cn) {
        contact_number = cn;
    }

    public void setAboutMe(String am) {
        about_me = am;
    }

    public void setNumberOfPets(String number_of_pets) {
        this.number_of_pets = number_of_pets;
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public String getContactNumber() {
        return contact_number;
    }

    public String getAboutMe() {
        return about_me;
    }

    public String getNumberOfPets() {
        return number_of_pets;
    }
}
