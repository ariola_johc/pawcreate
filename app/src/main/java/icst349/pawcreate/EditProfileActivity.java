package icst349.pawcreate;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

public class EditProfileActivity extends AppCompatActivity {
    DatabaseReference mDatabase;
    FirebaseAuth auth;
    StorageReference storageReference;
    private int PICK_IMAGE_REQUEST = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);

        auth = FirebaseAuth.getInstance();

        if(auth.getCurrentUser() == null)
            startActivity(new Intent(EditProfileActivity.this, LoginActivity.class));

        String location = "users/" + auth.getCurrentUser().getUid();
        mDatabase = FirebaseDatabase.getInstance().getReference(location);

        storageReference = FirebaseStorage.getInstance()
                .getReference()
                .child("users")
                .child(auth.getCurrentUser().getUid() + ".jpg");

        final EditText edit_name = (EditText) findViewById(R.id.owner_name);
        final EditText edit_contact = (EditText) findViewById(R.id.contact_number);
        final EditText edit_about = (EditText) findViewById(R.id.about_me);
        final Button btn_save = (Button) findViewById(R.id.save);
        final Button btn_cancel = (Button) findViewById(R.id.cancel);
        final ImageView iv_profile = (ImageView) findViewById(R.id.profile_picture);

        mDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                User user = dataSnapshot.getValue(User.class);

                edit_name.setText(user.getName());
                edit_contact.setText(user.getContactNumber());
                edit_about.setText(user.getAboutMe());
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                System.out.println("The read failed: " + databaseError.getCode());
            }
        });

        final long ONE_MEGABYTE = 1024 * 1024;
        storageReference.getBytes(ONE_MEGABYTE).addOnSuccessListener(new OnSuccessListener<byte[]>() {
            @Override
            public void onSuccess(byte[] bytes) {
                Bitmap bmp = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);

                iv_profile.setImageBitmap(
                        Bitmap.createScaledBitmap(
                                bmp,
                                iv_profile.getWidth(),
                                iv_profile.getHeight(),
                                false
                        )
                );
            }
        });

        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        btn_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String uid = auth.getCurrentUser().getUid();
                mDatabase = FirebaseDatabase.getInstance().getReference("/users/" + uid);

                if (TextUtils.isEmpty(edit_name.getText().toString())) {
                    Toast.makeText(
                            getApplicationContext(),
                            "Name cannot be empty.",
                            Toast.LENGTH_SHORT).show();

                    return;
                }
                mDatabase.child("name").setValue(edit_name.getText().toString());
                mDatabase.child("aboutMe").setValue(edit_about.getText().toString());
                mDatabase.child("contactNumber").setValue(edit_contact.getText().toString());

                iv_profile.setDrawingCacheEnabled(true);
                iv_profile.buildDrawingCache();
                Bitmap bitmap = iv_profile.getDrawingCache();
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
                byte[] data = baos.toByteArray();


                UploadTask uploadTask = storageReference.putBytes(data);
                uploadTask.addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Toast.makeText(
                                getApplicationContext(),
                                "Upload failed!",
                                Toast.LENGTH_SHORT
                        ).show();
                    }
                });

                finish();
//                startActivity(new Intent(EditProfileActivity.this, OwnerProfileActivity.class));
            }
        });

        iv_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();

                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST);
            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {

            Uri uri = data.getData();

            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);
                // Log.d(TAG, String.valueOf(bitmap));

                ImageView imageView = (ImageView) findViewById(R.id.profile_picture);
                imageView.setImageBitmap(bitmap);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
