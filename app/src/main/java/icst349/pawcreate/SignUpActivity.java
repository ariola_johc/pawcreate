package icst349.pawcreate;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class SignUpActivity extends AppCompatActivity {
    private FirebaseAuth auth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        auth = FirebaseAuth.getInstance();
        if (auth.getCurrentUser() != null)
            startActivity(new Intent(SignUpActivity.this, NavigationDrawerActivity.class));

        //redirect if already logged in
        if(auth.getCurrentUser() != null) {
            startActivity(new Intent(SignUpActivity.this, NavigationDrawerActivity.class));
        }

        final Button sign_up = (Button) findViewById(R.id.signup_button);
        final EditText edit_email = (EditText) findViewById(R.id.email_in);
        final EditText edit_password = (EditText) findViewById(R.id.pass_in);
        final EditText edit_conf_pass = (EditText) findViewById(R.id.confirm_pass_in);

        sign_up.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String email = edit_email.getText().toString().trim();
                String pass = edit_password.getText().toString().trim();
                String conf_pass = edit_conf_pass.getText().toString().trim();

                if(TextUtils.isEmpty(email)) {
                    Toast.makeText(
                            getApplicationContext(),
                            getString(R.string.email_empty),
                            Toast.LENGTH_SHORT)
                            .show();
                    return;
                }

                if(TextUtils.isEmpty(pass)) {
                    Toast.makeText(
                            getApplicationContext(),
                            getString(R.string.pass_empty),
                            Toast.LENGTH_SHORT)
                            .show();
                    return;
                }

                if(TextUtils.isEmpty(conf_pass)) {
                    Toast.makeText(
                            getApplicationContext(),
                            getString(R.string.conf_pass_empty),
                            Toast.LENGTH_SHORT)
                            .show();
                    return;
                }

                if(pass.length() < 6) {
                    Toast.makeText(
                            getApplicationContext(),
                            getString(R.string.minimum_password),
                            Toast.LENGTH_SHORT)
                            .show();
                    return;
                }

                if(!pass.equals(conf_pass)) {
                    Toast.makeText(
                            getApplicationContext(),
                            getString(R.string.conf_pass_mismatch),
                            Toast.LENGTH_SHORT).show();

                    return;
                }

                //creation of user
                auth.createUserWithEmailAndPassword(email, pass)
                        .addOnCompleteListener(SignUpActivity.this, new OnCompleteListener<AuthResult>() {
                            @Override
                            public void onComplete(@NonNull Task<AuthResult> task) {
                                String toast_message;

                                toast_message = ((task.isSuccessful())?"Successfully signed up!" : "Sign up error.");

                                Toast.makeText(getApplicationContext(),
                                        toast_message,
                                        Toast.LENGTH_SHORT).show();

                                if(!task.isSuccessful()) {
                                    Toast.makeText(
                                            getApplicationContext(),
                                            "Authentication failed. " + task.getException(),
                                            Toast.LENGTH_SHORT).show();
                                }
                                else {
                                    Toast.makeText(
                                            getApplicationContext(),
                                            "Please fill in your details.",
                                            Toast.LENGTH_SHORT).show();

                                    startActivity(new Intent(SignUpActivity.this, NewProfileActivity.class));
                                    finish();
                                }
                            }
                        });
            }
        });
    }
}
