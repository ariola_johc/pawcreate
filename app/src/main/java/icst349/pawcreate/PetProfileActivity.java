package icst349.pawcreate;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import org.w3c.dom.Text;

import java.sql.Array;

public class PetProfileActivity extends AppCompatActivity {

    FirebaseAuth auth;
    DatabaseReference reference;
    StorageReference storageReference;
    String pet_index;
    String[] gender_array;
    String[] specie_array;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pet_profile);

        auth = FirebaseAuth.getInstance();

        if(auth.getCurrentUser() == null)
            startActivity(new Intent(PetProfileActivity.this, LoginActivity.class));

        pet_index = getIntent().getStringExtra("pet_num");

        reference = FirebaseDatabase.getInstance()
                .getReference()
                .child("pets")
                .child(auth.getCurrentUser().getUid()).child(pet_index);

        storageReference = FirebaseStorage.getInstance()
                .getReference()
                .child("pets")
                .child(auth.getCurrentUser().getUid()).child(pet_index + ".jpg");

        final RelativeLayout rl_content = (RelativeLayout) findViewById(R.id.content);
        final ProgressBar progressBar = (ProgressBar) findViewById(R.id.progress);
        final TextView tv_pet_name = (TextView) findViewById(R.id.pet_name);
        final TextView tv_breed = (TextView) findViewById(R.id.breed);
        final TextView tv_birthday = (TextView) findViewById(R.id.birthday);
        final TextView tv_gender = (TextView) findViewById(R.id.gender);
        final TextView tv_about = (TextView) findViewById(R.id.about);
        final TextView tv_papers = (TextView) findViewById(R.id.papers);
        final TextView tv_specie = (TextView) findViewById(R.id.specie);
        final ImageView iv_profile_picture = (ImageView) findViewById(R.id.profile_picture);
        final long ONE_MEGABYTE = 1024 * 1024;

        gender_array = getResources().getStringArray(R.array.gender);
        specie_array = getResources().getStringArray(R.array.gender);
        rl_content.setVisibility(View.VISIBLE);
        progressBar.setVisibility(View.GONE);

        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Pet pet = dataSnapshot.getValue(Pet.class);

                tv_pet_name.setText(pet.getName());
                tv_breed.setText(pet.getBreed());
                tv_birthday.setText(pet.getBirthday());
                tv_gender.setText(gender_array[pet.getGender()]);
                tv_about.setText(pet.getAbout());
                tv_papers.setText(pet.getPapers());
                tv_specie.setText(specie_array[pet.getSpecie()]);

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                System.out.println("The read failed: " + databaseError.getCode());
            }
        });

        storageReference.getBytes(ONE_MEGABYTE).addOnSuccessListener(new OnSuccessListener<byte[]>() {
            @Override
            public void onSuccess(byte[] bytes) {
                Bitmap bmp = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);

                iv_profile_picture.setImageBitmap(
                        Bitmap.createScaledBitmap(
                                bmp,
                                iv_profile_picture.getWidth(),
                                iv_profile_picture.getHeight(),
                                false
                        )
                );
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.edit_profile, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_edit) {
            startActivity(new Intent(PetProfileActivity.this, EditPetProfileActivity.class)
                    .putExtra("pet_num", pet_index));
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
