package icst349.pawcreate;


import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.ThumbnailUtils;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import org.w3c.dom.Text;

import java.util.Random;


/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends Fragment {

    FirebaseAuth auth;
    DatabaseReference reference;
    DatabaseReference userReference;
    DatabaseReference pet_reference;
    StorageReference storageReference;
    String num_pets;
    String[] keys;
    int index;
    int pet_index;
    Pet pet;
    Random random;

    public HomeFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_home, container, false);


        final String[] PET_NAMES = {
                "Norbs", "Ris", "Danny", "Charm", "Pasir",
                "Debbie", "Hans", "Lance", "Clarke", "Quay"
        };

        final int[] PET_DISTANCES = {
                23, 7, 32, 18, 11,
                10, 15, 20, 12, 17
        };

        final ImageView btn_like = (ImageView) v.findViewById(R.id.button_like);
        final ImageView btn_dislike = (ImageView) v.findViewById(R.id.button_dislike);
        final ImageView iv_pet_profile_picture = (ImageView) v.findViewById(R.id.pet_profile_picture);
        final TextView tv_petname = (TextView) v.findViewById(R.id.home_pet_name);
        final TextView tv_petdist = (TextView) v.findViewById(R.id.home_pet_distance);
        final RelativeLayout rl_petchooser = (RelativeLayout) v.findViewById(R.id.pet_chooser);
        final ProgressBar progressBar = (ProgressBar) v.findViewById(R.id.progress);
        final long ONE_MEGABYTE = 1024 * 1024;
        reference = FirebaseDatabase.getInstance().getReference().child("users");
        userReference = FirebaseDatabase.getInstance().getReference().child("users");
        pet_reference = FirebaseDatabase.getInstance().getReference().child("pets");
        storageReference = FirebaseStorage.getInstance().getReference().child("pets");
        auth = FirebaseAuth.getInstance();
        pet_index = 0;


        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                int size = (int)(dataSnapshot.getChildrenCount());
                keys = new String[size];
                int ref_index = 0;
                for(DataSnapshot u : dataSnapshot.getChildren()) {
                    keys[ref_index] = u.getKey();
                    ref_index ++;
                }
                random = new Random();

                do {
                    index = random.nextInt(size);
                } while (auth.getCurrentUser().getUid().equals(keys[index]));

                num_pets = "0";

                userReference.child(keys[index]).child("numberOfPets").addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        num_pets = dataSnapshot.getValue(String.class);
                        pet_index = random.nextInt(Integer.parseInt(num_pets));

                        pet_reference
                                .child(keys[index])
                                .child(Integer.toString(pet_index))
                                .addValueEventListener(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(DataSnapshot dataSnapshot) {
                                        pet = dataSnapshot.getValue(Pet.class);
                                        tv_petname.setText(pet.getName());
                                    }

                                    @Override
                                    public void onCancelled(DatabaseError databaseError) {
                                        System.out.println("The read failed: " + databaseError.getCode());
                                    }
                                });

                        storageReference
                                .child(keys[index])
                                .child(Integer.toString(pet_index) + ".jpg")
                                .getBytes(ONE_MEGABYTE).addOnSuccessListener(new OnSuccessListener<byte[]>() {
                            @Override
                            public void onSuccess(byte[] bytes) {
                                Bitmap bmp = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
                                bmp = cropCenter(bmp);

                                iv_pet_profile_picture.setImageBitmap(
                                        Bitmap.createScaledBitmap(
                                                bmp,
                                                (int) getResources().getDimension(R.dimen.image_size),
                                                (int) getResources().getDimension(R.dimen.image_size),
                                                false
                                        )
                                );

                                progressBar.setVisibility(View.GONE);
                                rl_petchooser.setVisibility(View.VISIBLE);
                            }
                        });
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        System.out.println("The read failed: " + databaseError.getCode());
                    }
                });
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                System.out.println("The read failed: " + databaseError.getCode());
            }
        });

        btn_like.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progressBar.setVisibility(View.VISIBLE);
                rl_petchooser.setVisibility(View.GONE);

                random = new Random();

                do {
                    index = random.nextInt(keys.length);
                } while (auth.getCurrentUser().getUid().equals(keys[index]));

                num_pets = "0";

                userReference.child(keys[index]).child("numberOfPets").addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        num_pets = dataSnapshot.getValue(String.class);
                        pet_index = random.nextInt(Integer.parseInt(num_pets));

                        pet_reference
                                .child(keys[index])
                                .child(Integer.toString(pet_index))
                                .addValueEventListener(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(DataSnapshot dataSnapshot) {
                                        pet = dataSnapshot.getValue(Pet.class);
                                        tv_petname.setText(pet.getName());
                                    }

                                    @Override
                                    public void onCancelled(DatabaseError databaseError) {
                                        System.out.println("The read failed: " + databaseError.getCode());
                                    }
                                });

                        storageReference
                                .child(keys[index])
                                .child(Integer.toString(pet_index) + ".jpg")
                                .getBytes(ONE_MEGABYTE).addOnSuccessListener(new OnSuccessListener<byte[]>() {
                            @Override
                            public void onSuccess(byte[] bytes) {
                                Bitmap bmp = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
                                bmp = cropCenter(bmp);

                                iv_pet_profile_picture.setImageBitmap(
                                        Bitmap.createScaledBitmap(
                                                bmp,
                                                (int) getResources().getDimension(R.dimen.image_size),
                                                (int) getResources().getDimension(R.dimen.image_size),
                                                false
                                        )
                                );

                                progressBar.setVisibility(View.GONE);
                                rl_petchooser.setVisibility(View.VISIBLE);
                            }
                        });
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        System.out.println("The read failed: " + databaseError.getCode());
                    }
                });
            }
        });

        btn_dislike.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progressBar.setVisibility(View.VISIBLE);
                rl_petchooser.setVisibility(View.GONE);

                random = new Random();

                do {
                    index = random.nextInt(keys.length);
                } while (auth.getCurrentUser().getUid().equals(keys[index]));

                num_pets = "0";

                userReference.child(keys[index]).child("numberOfPets").addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        num_pets = dataSnapshot.getValue(String.class);
                        pet_index = random.nextInt(Integer.parseInt(num_pets));

                        pet_reference
                                .child(keys[index])
                                .child(Integer.toString(pet_index))
                                .addValueEventListener(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(DataSnapshot dataSnapshot) {
                                        pet = dataSnapshot.getValue(Pet.class);
                                        tv_petname.setText(pet.getName());
                                    }

                                    @Override
                                    public void onCancelled(DatabaseError databaseError) {
                                        System.out.println("The read failed: " + databaseError.getCode());
                                    }
                                });

                        storageReference
                                .child(keys[index])
                                .child(Integer.toString(pet_index) + ".jpg")
                                .getBytes(ONE_MEGABYTE).addOnSuccessListener(new OnSuccessListener<byte[]>() {
                            @Override
                            public void onSuccess(byte[] bytes) {
                                Bitmap bmp = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
                                bmp = cropCenter(bmp);

                                iv_pet_profile_picture.setImageBitmap(
                                        Bitmap.createScaledBitmap(
                                                bmp,
                                                (int) getResources().getDimension(R.dimen.image_size),
                                                (int) getResources().getDimension(R.dimen.image_size),
                                                false
                                        )
                                );

                                progressBar.setVisibility(View.GONE);
                                rl_petchooser.setVisibility(View.VISIBLE);
                            }
                        });
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        System.out.println("The read failed: " + databaseError.getCode());
                    }
                });
            }
        });

        return v;
    }

    public static Bitmap cropCenter(Bitmap bmp) {
        int dimension = Math.min(bmp.getWidth(), bmp.getHeight());
        return ThumbnailUtils.extractThumbnail(bmp, dimension, dimension);
    }

}
