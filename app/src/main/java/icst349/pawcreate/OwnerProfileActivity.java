package icst349.pawcreate;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.ThumbnailUtils;
import android.support.annotation.NonNull;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import org.w3c.dom.Text;

import java.net.NoRouteToHostException;

public class OwnerProfileActivity extends AppCompatActivity {

    int i;
    DatabaseReference mDatabase;
    DatabaseReference pet_reference;
    StorageReference storageReference;
    StorageReference pet_pic_reference;
    FirebaseAuth auth;
    User user;
    int pet_number;
    int buffer;
    Pet pet;
    TextView tv_pet;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        auth = FirebaseAuth.getInstance();

        if(auth.getCurrentUser() == null)
            startActivity(new Intent(OwnerProfileActivity.this, LoginActivity.class));

        String location = "users/" + auth.getCurrentUser().getUid();
        final String pet_location = "pets/" + auth.getCurrentUser().getUid() + "-";
        mDatabase = FirebaseDatabase.getInstance().getReference(location);
        storageReference = FirebaseStorage.getInstance()
                .getReference()
                .child("users")
                .child(auth.getCurrentUser().getUid() + ".jpg");

        pet_pic_reference = FirebaseStorage.getInstance()
                .getReference()
                .child("pets")
                .child(auth.getCurrentUser().getUid());

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_owner_profile);

        LinearLayout add_pet = (LinearLayout) findViewById(R.id.add_pet_area);
        final TextView tv_name = (TextView) findViewById(R.id.owner_name);
        final TextView tv_about = (TextView) findViewById(R.id.about_me);
        final TextView tv_contact = (TextView) findViewById(R.id.contact_no);
        final LinearLayout ll_petarea = (LinearLayout) findViewById(R.id.pet_area);
        final RelativeLayout rl_picture_area = (RelativeLayout) findViewById(R.id.picture_area);
        final ScrollView sv_information_area = (ScrollView) findViewById(R.id.information_area);
        final ProgressBar progressBar = (ProgressBar) findViewById(R.id.progress);
        final ImageView iv_profile = (ImageView) findViewById(R.id.profile_picture);
        final long ONE_MEGABYTE = 1024 * 1024;
        tv_pet = null;

        storageReference.getBytes(ONE_MEGABYTE).addOnSuccessListener(new OnSuccessListener<byte[]>() {
            @Override
            public void onSuccess(byte[] bytes) {
                Bitmap bmp = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);

                iv_profile.setImageBitmap(
                        Bitmap.createScaledBitmap(
                                bmp,
                                iv_profile.getWidth(),
                                iv_profile.getHeight(),
                                false
                        )
                );
            }
        });

        add_pet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(OwnerProfileActivity.this, AddPetActivity.class));
            }
        });

        mDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                progressBar.setVisibility(View.VISIBLE);
                rl_picture_area.setVisibility(View.GONE);
                sv_information_area.setVisibility(View.GONE);

                user = dataSnapshot.getValue(User.class);

                tv_name.setText(user.getName());
                tv_about.setText(user.getAboutMe());
                tv_contact.setText(user.getContactNumber());
                pet_number = Integer.parseInt(user.getNumberOfPets());

                progressBar.setVisibility(View.GONE);
                rl_picture_area.setVisibility(View.VISIBLE);
                sv_information_area.setVisibility(View.VISIBLE);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                //TODO: System.out.println("The read failed: " + databaseError.getCode());
                System.out.println("The read failed: " + databaseError.getCode());
            }
        });

        pet_reference = FirebaseDatabase
                .getInstance()
                .getReference()
                .child("pets/")
                .child(auth.getCurrentUser().getUid());

//        pet_reference.addValueEventListener(new ValueEventListener() {
//            @Override
//            public void onDataChange(DataSnapshot dataSnapshot) {
//
//                for(DataSnapshot p : dataSnapshot.getChildren()) {
//                    pet = p.getValue(Pet.class);
//                }
//            }
//
//            @Override
//            public void onCancelled(DatabaseError databaseError) {
//                System.out.println("The read failed: " + databaseError.getCode());
//            }
//        });

        pet_reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                progressBar.setVisibility(View.VISIBLE);
                rl_picture_area.setVisibility(View.GONE);
                sv_information_area.setVisibility(View.GONE);
                ll_petarea.removeAllViews();
                i = 0;
                for(DataSnapshot p : dataSnapshot.getChildren()) {
                    final String key = p.getKey();
                    pet = p.getValue(Pet.class);
                    final ImageView iv_pet_prof = new ImageView(getApplicationContext());
                    final LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
                            (int) getResources().getDimension(R.dimen.square_image),
                            (int) getResources().getDimension(R.dimen.square_image));
                    iv_pet_prof.setLayoutParams(layoutParams);
                    iv_pet_prof.setScaleType(ImageView.ScaleType.CENTER_CROP);

                    pet_pic_reference.child(Integer.toString(i) + ".jpg").getBytes(ONE_MEGABYTE).addOnSuccessListener(new OnSuccessListener<byte[]>() {
                        @Override
                        public void onSuccess(byte[] bytes) {

                            Bitmap bmp = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
                            bmp = cropCenter(bmp);
                            iv_pet_prof.setImageBitmap(
                                    Bitmap.createScaledBitmap(
                                            bmp,
                                            (int) getResources().getDimension(R.dimen.square_image),
                                            (int) getResources().getDimension(R.dimen.square_image),
                                            false
                                    )
                            );
                            iv_pet_prof.setLayoutParams(layoutParams);
                        }
                    });

                    LinearLayout buffer = new LinearLayout(OwnerProfileActivity.this);
                    buffer.setOrientation(LinearLayout.VERTICAL);

                    LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                            LinearLayout.LayoutParams.WRAP_CONTENT,
                            LinearLayout.LayoutParams.WRAP_CONTENT
                    );

                    params.setMargins(0, 0, 20, 0);

                    buffer.setLayoutParams(params);

                    tv_pet = new TextView(OwnerProfileActivity.this);
                    tv_pet.setText(pet.getName());

                    buffer.addView(iv_pet_prof);
                    buffer.addView(tv_pet);
                    buffer.setGravity(Gravity.CENTER);

                    buffer.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            startActivity(
                                    new Intent(OwnerProfileActivity.this, PetProfileActivity.class)
                                            .putExtra("pet_num", key)
                            );
                        }
                    });

                    ll_petarea.addView(buffer);
                    i++;
                }

                progressBar.setVisibility(View.GONE);
                rl_picture_area.setVisibility(View.VISIBLE);
                sv_information_area.setVisibility(View.VISIBLE);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                System.out.println("The read failed: " + databaseError.getCode());
            }

        });


    }

    public static Bitmap cropCenter(Bitmap bmp) {
        int dimension = Math.min(bmp.getWidth(), bmp.getHeight());
        return ThumbnailUtils.extractThumbnail(bmp, dimension, dimension);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.edit_profile, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_edit) {
            startActivity(new Intent(OwnerProfileActivity.this, EditProfileActivity.class));
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
