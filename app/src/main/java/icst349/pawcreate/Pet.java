package icst349.pawcreate;

/**
 * Created by Carlo on 2017-03-14.
 */

public class Pet {
    String name;
    int specie;
    String breed;
    String birthday;
    int gender;
    String about;
    String papers;

    Pet(){
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSpecie(int specie) {
        this.specie = specie;
    }

    public void setBreed(String breed) {
        this.breed = breed;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public void setGender(int gender) {
        this.gender = gender;
    }

    public void setAbout(String about) {
        this.about = about;
    }

    public void setPapers(String papers) {
        this.papers = papers;
    }

    public String getName() {
        return name;
    }

    public String getBirthday() {
        return birthday;
    }

    public String getBreed() {
        return breed;
    }

    public int getGender() {
        return gender;
    }

    public int getSpecie() {
        return specie;
    }

    public String getAbout() {
        return about;
    }

    public String getPapers() {
        return papers;
    }
}
