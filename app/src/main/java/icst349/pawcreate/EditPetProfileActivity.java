package icst349.pawcreate;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

public class EditPetProfileActivity extends AppCompatActivity {

    FirebaseAuth auth;
    DatabaseReference reference;
    DatabaseReference edit_reference;
    StorageReference storageReference;
    String pet_index;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_pet_profile);

        auth = FirebaseAuth.getInstance();

        if(auth.getCurrentUser() == null)
            startActivity(new Intent(EditPetProfileActivity.this, LoginActivity.class));

        pet_index = getIntent().getStringExtra("pet_num");

        reference = FirebaseDatabase.getInstance()
                .getReference()
                .child("pets")
                .child(auth.getCurrentUser().getUid())
                .child(pet_index);

        storageReference = FirebaseStorage.getInstance()
                .getReference()
                .child("pets")
                .child(auth.getCurrentUser().getUid())
                .child(pet_index + ".jpg");

        Button save = (Button) findViewById(R.id.save);
        Button cancel = (Button) findViewById(R.id.cancel);
        final Spinner s_specie = (Spinner) findViewById(R.id.specie_spinner);
        final Spinner s_gender = (Spinner) findViewById(R.id.gender_spinner);
        final EditText et_birthday = (EditText) findViewById(R.id.birthday);
        final EditText et_name = (EditText) findViewById(R.id.petName);
        final EditText et_breed = (EditText) findViewById(R.id.petBreed);
        final EditText et_about = (EditText) findViewById(R.id.about);
        final EditText et_papers = (EditText) findViewById(R.id.papers);

        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Pet pet = dataSnapshot.getValue(Pet.class);

                s_specie.setSelection(pet.getSpecie());
                s_gender.setSelection(pet.getGender());
                et_birthday.setText(pet.getBirthday());
                et_name.setText(pet.getName());
                et_breed.setText(pet.getBreed());
                et_about.setText(pet.getAbout());
                et_papers.setText(pet.getPapers());

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                System.out.println("The read failed: " + databaseError.getCode());
            }
        });

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Pet pet = new Pet();
                reference.child("specie").setValue(s_specie.getSelectedItemPosition());
                reference.child("gender").setValue(s_gender.getSelectedItem());
                reference.child("birthday").setValue(et_birthday.getText().toString());
                reference.child("name").setValue(et_name.getText().toString());
                reference.child("breed").setValue(et_breed.getText().toString());
                reference.child("about").setValue(et_about.getText().toString());
                reference.child("papers").setValue(et_papers.getText().toString());

//                s_specie.getSelectedItemPosition();
//                s_gender.getSelectedItem();
//                et_birthday.getText().toString();
//                et_name.getText().toString();
//                et_breed.getText().toString();
//                et_about.getText().toString();
//                et_papers.getText().toString();
                finish();
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
