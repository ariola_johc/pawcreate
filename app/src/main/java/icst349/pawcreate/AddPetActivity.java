package icst349.pawcreate;

import android.content.Intent;
import android.graphics.Bitmap;
import android.media.Image;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

public class AddPetActivity extends AppCompatActivity {

    FirebaseAuth auth;
    DatabaseReference reference;
    StorageReference storageReference;
    String location;
    int index;
    boolean has_been_set;
    int PICK_IMAGE_REQUEST = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_pet);

        auth = FirebaseAuth.getInstance();

        if(auth.getCurrentUser() == null)
            startActivity(new Intent(AddPetActivity.this, LoginActivity.class));

        location = "users/" + auth.getCurrentUser().getUid();
        reference = FirebaseDatabase.getInstance().getReference(location);
        storageReference = FirebaseStorage.getInstance().getReference().child("pets").child(auth.getCurrentUser().getUid());

        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                User user = dataSnapshot.getValue(User.class);
                index = Integer.parseInt(user.getNumberOfPets());
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                System.out.println("The read failed: " + databaseError.getCode());
            }
        });

        final Spinner spin_specie = (Spinner) findViewById(R.id.specie_spinner);
        final Spinner spin_gender = (Spinner) findViewById(R.id.gender_spinner);
        final EditText edit_date = (EditText) findViewById(R.id.birthday);
        final EditText edit_pet_name = (EditText) findViewById(R.id.petName);
        final EditText edit_pet_breed = (EditText) findViewById(R.id.petBreed);
        final EditText edit_about = (EditText) findViewById(R.id.about);
        final EditText edit_papers = (EditText) findViewById(R.id.papers);
        final ImageView iv_profile = (ImageView) findViewById(R.id.profile_picture);

        Button save = (Button) findViewById(R.id.save);
        Button cancel = (Button) findViewById(R.id.cancel);

        iv_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();

                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST);
            }
        });

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int specie = spin_specie.getSelectedItemPosition();
                int gender = spin_gender.getSelectedItemPosition();
                String birthday = edit_date.getText().toString();
                String pet_name = edit_pet_name.getText().toString();
                String pet_breed = edit_pet_breed.getText().toString();
                String about = edit_about.getText().toString();
                String papers = edit_papers.getText().toString();

                if(TextUtils.isEmpty(pet_name)) {
                    Toast.makeText(
                            getApplicationContext(),
                            "Pet name cannot be empty",
                            Toast.LENGTH_SHORT)
                            .show();

                    return;
                }

                Pet pet = new Pet();

                pet.setName(pet_name);
                pet.setAbout(about);
                pet.setBirthday(birthday);
                pet.setSpecie(specie);
                pet.setGender(gender);
                pet.setBreed(pet_breed);
                pet.setPapers(papers);

                DatabaseReference pet_reference = FirebaseDatabase.getInstance().getReference();

                iv_profile.setDrawingCacheEnabled(true);
                iv_profile.buildDrawingCache();
                Bitmap bitmap = iv_profile.getDrawingCache();
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
                byte[] data = baos.toByteArray();

                pet_reference.child("pets").child(auth.getCurrentUser().getUid() + "/" + Integer.toString(index)).setValue(pet);

                UploadTask uploadTask = storageReference
                        .child(Integer.toString(index) + ".jpg")
                        .putBytes(data);

                uploadTask.addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Toast.makeText(
                                getApplicationContext(),
                                "Upload failed!",
                                Toast.LENGTH_SHORT
                        ).show();
                    }
                });
                index++;

                reference.child("numberOfPets").setValue(Integer.toString(index));
                finish();
            }
        }
        );

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {

            Uri uri = data.getData();

            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);
                // Log.d(TAG, String.valueOf(bitmap));

                ImageView imageView = (ImageView) findViewById(R.id.profile_picture);
                imageView.setImageBitmap(bitmap);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
