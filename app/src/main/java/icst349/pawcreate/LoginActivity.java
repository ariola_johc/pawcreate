package icst349.pawcreate;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class LoginActivity extends AppCompatActivity {
    private FirebaseAuth auth;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        auth = FirebaseAuth.getInstance();

        if (auth.getCurrentUser() != null) {
            startActivity(new Intent(LoginActivity.this, NavigationDrawerActivity.class));
        }
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        final EditText edit_email = (EditText) findViewById(R.id.email_in);
        final EditText edit_pass = (EditText) findViewById(R.id.pass_in);

        final Button login =(Button) findViewById(R.id.button_login);
        final Button signup =(Button) findViewById(R.id.button_signup);
        final Button forgot =(Button) findViewById(R.id.button_forgot);

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String email = edit_email.getText().toString();
                final String pass = edit_pass.getText().toString();

                if(TextUtils.isEmpty(email)) {
                    Toast.makeText(
                            getApplicationContext(),
                            getString(R.string.email_empty),
                            Toast.LENGTH_SHORT)
                            .show();
                    return;
                }

                if(TextUtils.isEmpty(pass)) {
                    Toast.makeText(
                            getApplicationContext(),
                            getString(R.string.pass_empty),
                            Toast.LENGTH_SHORT)
                            .show();
                    return;
                }

                auth.signInWithEmailAndPassword(email, pass)
                        .addOnCompleteListener(LoginActivity.this, new OnCompleteListener<AuthResult>() {
                            @Override
                            public void onComplete(@NonNull Task<AuthResult> task) {
                                if(!task.isSuccessful()) {
                                    if(pass.length() < 6) {
                                        edit_pass.setError(getString(R.string.minimum_password));
                                    } else {
                                        Toast.makeText(
                                                LoginActivity.this,
                                                getString(R.string.auth_failed),
                                                Toast.LENGTH_LONG)
                                                .show();
                                    }
                                } else {
                                    startActivity(new Intent(LoginActivity.this, NavigationDrawerActivity.class));
                                    finish();
                                }
                            }
                        });

//                startActivity(new Intent(LoginActivity.this, NavigationDrawerActivity.class));
            }
        });

        signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(LoginActivity.this, SignUpActivity.class));
            }
        });

        forgot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(LoginActivity.this, ForgotPasswordActivity.class));
            }
        });

    }
}
