package icst349.pawcreate;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.opengl.EGLDisplay;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import org.w3c.dom.Text;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

public class NewProfileActivity extends AppCompatActivity {
    private FirebaseUser fbuser;
    private User user;
    private FirebaseAuth auth;
    private DatabaseReference mDatabase;
    private StorageReference storageReference;
    private int PICK_IMAGE_REQUEST = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        user = new User();
        auth = FirebaseAuth.getInstance();
        mDatabase = FirebaseDatabase.getInstance().getReference();
        storageReference = FirebaseStorage.getInstance()
                .getReference()
                .child("users")
                .child(auth.getCurrentUser().getUid() + ".jpg");

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_profile);

        final Button save = (Button) findViewById(R.id.save);
        final EditText edit_name = (EditText) findViewById(R.id.owner_name);
        final EditText edit_contact = (EditText) findViewById(R.id.contact);
        final EditText edit_about_me = (EditText) findViewById(R.id.about_me);
        final ImageView iv_profile = (ImageView) findViewById(R.id.profile_picture);

        iv_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();

                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST);
            }
        });

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name = edit_name.getText().toString();
                String contact = edit_contact.getText().toString();
                String about_me = edit_about_me.getText().toString();

                if(TextUtils.isEmpty(name)) {
                    Toast.makeText(getApplicationContext(),
                            "Name cannot be blank",
                            Toast.LENGTH_SHORT)
                            .show();
                }

                if(TextUtils.isEmpty(contact)){
                    contact = "";
                }

                if(TextUtils.isEmpty(about_me)){
                    about_me = "";
                }

                user.setName(name);
                user.setContactNumber(contact);
                user.setAboutMe(about_me);
                user.setNumberOfPets("0");

                mDatabase.child("users").child(auth.getCurrentUser().getUid()).setValue(user);

                iv_profile.setDrawingCacheEnabled(true);
                iv_profile.buildDrawingCache();
                Bitmap bitmap = iv_profile.getDrawingCache();
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
                byte[] data = baos.toByteArray();

                UploadTask uploadTask = storageReference.putBytes(data);
                uploadTask.addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Toast.makeText(
                                getApplicationContext(),
                                "Upload failed!",
                                Toast.LENGTH_SHORT
                        ).show();
                    }
                }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                        Toast.makeText(
                                getApplicationContext(),
                                "Welcome to Pawcreate!",
                                Toast.LENGTH_SHORT
                        ).show();
                    }
                });

                startActivity(new Intent(NewProfileActivity.this, NavigationDrawerActivity.class));
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {

            Uri uri = data.getData();

            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);
                // Log.d(TAG, String.valueOf(bitmap));

                ImageView imageView = (ImageView) findViewById(R.id.profile_picture);
                imageView.setImageBitmap(bitmap);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
