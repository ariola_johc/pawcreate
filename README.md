#Pawcreate

###Version 0.3.0
* Can now randomize through all the pets available in the database.
* Can now view a pet profile.
* Minor fixes on the pet profile
* **DO NOT GO TO EDIT PET PROFILE I REPEAT DO NOT GO THERE AND EDIT, IT WILL BREAK YOUR ACCOUNT.**

###Version 0.2.4
* Added functionality to add picture to user profile and pet profile.
* Modified Owner Profile so that the user can see their pets (No link yet).

###Version 0.2.3
* Added functionality for adding pets.
* Added loading prompt when retrieving data from Firebase on OwnerProfileActivity
* Added functionality for showing pet names on Owner's Profile

###Version 0.2.2
* Modified Add Pet View and Edit Pet View.
* Added functionality for user to edit his/her profile.
* Tried to add functionality for add pet but failed miserably.

###Version 0.2.1
* Added Firebase Database functionality successfully.
* Added the user profile functionality, although you could not edit it yet.
* Note: Fix slow response from the database.
* Added the NewProfileActivity for instantiating profile when you sign up

###Version 0.2.0
* Added Firebase into application
* Added the following functionalities:
 * Sign - Up
 * Login
 * Logout

###Version 0.1.2
* Hard Backup
 
###Version 0.1.1
* Added content for home activity
 
###Version 0.1.0
* Added Navigation Drawer
* Connected the following activities:
 * Add Pet
 * Pet Profile
 * Edit Pet Profile

###Version 0.0.4
* Added layouts for the following activities:
 * Add Pet
 * Pet Profile (currently just hardcoded)
 * Edit Pet Profile

####Known Bugs:
 * No Option Menu Button showing on older devices.
 * Expanding EditText on EditPet and AddPet when input is overflowing.
